package com.example.excercise1thomaslao;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.excercise1thomaslao.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {


    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //inflate the layouts
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.Calculate.setOnClickListener(this::onClick);

    }

    public void onClick(View view) {
        //if you entered nothing, a toast appears
        if (binding.textView.getText().length() == 0) {
            Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_LONG).show();
            return;
        }

        //Get input value from text box
        float inputValue = Float.parseFloat(binding.textView.getText().toString());
        if (binding.celsiusBtn.isChecked()) {
            binding.textView.setText(String.valueOf(ConverterUtil.convertFahrenheitToCelsius(inputValue)));
            //Switch to other option
            binding.celsiusBtn.setChecked(false);
            binding.fahrenheitBtn.setChecked(true);
        } else {
            binding.textView.setText(String.valueOf(ConverterUtil.convertCelsiusToFahrenheit(inputValue)));
            binding.celsiusBtn.setChecked(true);
            binding.fahrenheitBtn.setChecked(false);
        }

    }

}